import React,{useEffect,useState} from "react";

import "./Notification.scss";

const Notification = ({message}) => {
  const [active,setActive] = useState(true)
  useEffect(()=>{
  let toogleActive =  setTimeout(()=>{
setActive(false)
  },2000)
  return ()=>{clearTimeout(toogleActive)}
  },[])
  const CLASS = "Notification";
  return (
    <div className={`${CLASS} ${!active ? 'hide' : ''}`}>
      <p>{message}</p>
    </div>
  );
};
export default Notification;
