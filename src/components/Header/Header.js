import React from "react";
import { FaBars } from "react-icons/fa";

import avatar from "../../images/unknownUser.jpg";

import "./Header.scss";

const Header = ({ setShowSidebar, showSidebar }) => {
  const CLASS = "Header";
  return (
    <header className={CLASS} >
      <button
        className={`${CLASS}__sidebarTrigger`}
        onClick={() => setShowSidebar(!showSidebar)}
      >
        <FaBars />
      </button>
      <div className={`${CLASS}__logo`}>
        <img
          src="https://anvkgjjben.cloudimg.io/width/400/x/https://img.innoloft.de/innoloft-no-white-space.svg"
          alt="Logo"
        />
      </div>
      <div className={`${CLASS}__menu`}>
        <span className={`${CLASS}__menu__item`}>EN</span>
        <span className={`${CLASS}__menu__item`}>
          <img src={avatar} alt="avatar" />
        </span>
      </div>
    </header>
  );
};
export default Header;
