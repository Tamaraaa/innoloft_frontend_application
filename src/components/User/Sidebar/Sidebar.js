import React from "react";
import {
  FaBullhorn,
  FaHome,
  FaBuilding,
  FaNewspaper,
  FaChartArea,
} from "react-icons/fa";
import { IoIosSettings } from "react-icons/io";

import avatar from "../../../images/unknownUser.jpg";

import "./Sidebar.scss";

const Sidebar = ({ showSidebar, setShowSidebar}) => {
  const CLASS = "Sidebar";
  return (
    <>
    <div onClick={()=>{setShowSidebar(false)}} className={`backdrop ${showSidebar ? "active" : ""}`} />
    <div className={`${CLASS} ${showSidebar ? "active" : ""} `}>
      <div className={`${CLASS}__user`}>
        <img src={avatar} alt="avatar" />
      </div>
      <nav className={`${CLASS}__nav`}>
        <ul>
          <li>
            <FaHome />
            <span> Home</span>
          </li>
          <li>
            <FaBullhorn />
            <span> My Account</span>
          </li>
          <li>
            <FaBuilding />
            <span> My Company</span>
          </li>
          <li>
            <IoIosSettings />
            <span> My Settings</span>
          </li>
          <li>
            <FaNewspaper />
            <span> News</span>
          </li>
          <li>
            <FaChartArea />
            <span>Analytics</span>
          </li>
        </ul>
      </nav>
    </div>
    </>
  );
};
export default Sidebar;
