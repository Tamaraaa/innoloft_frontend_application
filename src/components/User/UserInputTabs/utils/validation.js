export const validate = (values) => {

  let errors = {};
  
  if ("email" in values) {
    if (!values.email) {
      errors.email = "Email address is required";
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
      errors.email = "Email address is invalid";
    }
  }
  if ("password" in values) {
    if (!values.password) {
      errors.password = "Password is required";
    } else if (values.password.length < 5) {
      errors.password = "Password must be 5 or more characters";
    }
  }
  if ("confirmedPassword" in values) {
    if (values.password !== values.confirmedPassword) {
      errors.confirmedPassword = "Password must match";
    }
  }
  if ("name" in values) {
    if (!values.name) {
      errors.name = "Name is required";
    } else if (values.name.length < 2) {
      errors.name = "Name must be 2 or more characters";
    }
  }
  if ("lastName" in values) {
    if (!values.lastName) {
      errors.lastName = "Last name is required";
    } else if (values.lastName.length < 2) {
      errors.lastName = "Last name must be 2 or more characters";
    }
  }
  if ("street" in values) {
    if (!values.street) {
      errors.street = "Street name is required";
    } else if (values.street.length < 2) {
      errors.street = "Street name must be 2 or more characters";
    }
  }
  if ("houseNum" in values) {
    if (!values.houseNum) {
      errors.houseNum = "Home number is required";
    }
  }

  if ("zip" in values) {
    if (!values.zip) {
      errors.zip = "zip code is required";
    }
  }

  return errors;
};
