import React, { useState } from "react";
import { connect } from "react-redux";
import { validate } from "./utils/validation";
import zxcvbn from "zxcvbn";

import { updateUserInfo } from "../../../redux/actions";

import "./UserInputTabs.scss";

const UserInputTabs = ({ updateUserInfo }) => {
  const CLASS = "UserInputTabs";
  const [strength, setStrength] = useState(0);
  const [error, setError] = useState({});
  const [activeTab, setActiveTab] = useState("mainInfo");
  const strengthRate = ["POOR", "WEEK", "OK", "GOOD", "STRONG"];
  const strengthColors = ["red", "orange", "yellow", "green", "blue"];

  const [emailData, setEmailData] = useState({
    email: '',
    password: '',
    confirmedPassword: '',
  });
  const [additionalInfo, setAdditionalInfo] = useState({
    name: "John",
    lastName: "Smith",
    street: "Test street",
    houseNum: 435,
    zip: 245,
    country: "Germany",
  });

  const handleBlur = (data) => {
    let errors = validate(data);
    setError(errors);
  };
  let dataToSubmit = activeTab === "mainInfo" ? emailData : additionalInfo;

  const reset = () => {
    setEmailData({
      email: "",
      password: "",
      confirmedPassword: "",
    });
    setAdditionalInfo({
      name: "John",
      lastName: "Smith",
      street: "Test street",
      houseNum: 435,
      zip: 245,
      country: "Germany",
    });
  };

  const handleMainInfoSubmit = (e, data) => {
    e.preventDefault();
    let errors = validate(data);
    if (!Object.keys(errors).length) {
      updateUserInfo(data);
      setEmailData({
        email: "",
        password: "",
        confirmedPassword: "",
      });
      setError({});
    } else {
      setError(errors);
    }
  };
  const mainInfoContent = (
    <>
      <div className={`${CLASS}__content__body__form__item`}>
        <input
          autoComplete="off"
          placeholder="Type in your new email"
          maxLength="100"
          value={emailData.email}
          onBlur={(e) => handleBlur({ email: e.target.value })}
          onChange={(e) =>
            setEmailData({ ...emailData, email: e.target.value })
          }
        />
        <p>{error.email && error.email}</p>
      </div>
      <div className={`${CLASS}__content__body__form__item`}>
        <input
          autoComplete="off"
          type="password"
          placeholder="Type in your new password"
          onBlur={(e) => handleBlur({ password: e.target.value })}
          maxLength="50"
          value={emailData.password}
          onChange={(e) => {
            setEmailData({ ...emailData, password: e.target.value });
            let valueStrength = zxcvbn(e.target.value);
            setStrength(valueStrength.score);
          }}
        />
        {emailData.password ? (
          <p style={{ color: strengthColors[strength] }}>
            Password strength:{strengthRate[strength]}
          </p>
        ) : (
          ""
        )}
        <p>{error.password && error.password}</p>
      </div>
      <div className={`${CLASS}__content__body__form__item`}>
        <input
          autoComplete="off"
          type="password"
          placeholder="Repeat password"
          value={emailData.confirmedPassword}
          onBlur={(e) => handleBlur({ confirmedPassword: e.target.value })}
          onChange={(e) =>
            setEmailData({ ...emailData, confirmedPassword: e.target.value })
          }
        />
        <p>{error.confirmedPassword && error.confirmedPassword}</p>
      </div>
    </>
  );
  const additionalInfoContent = (
    <>
      <div className={`${CLASS}__content__body__form__item`}>
        <input
          placeholder="Your First Name"
          maxLength="50"
          value={additionalInfo.name}
          onBlur={(e) => handleBlur({ name: e.target.value })}
          onChange={(e) =>
            setAdditionalInfo({ ...additionalInfo, name: e.target.value })
          }
        />
        <p>{error.name && error.name}</p>
      </div>
      <div className={`${CLASS}__content__body__form__item`}>
        <input
          placeholder="Your Last Name"
          maxLength="50"
          value={additionalInfo.lastName}
          onBlur={(e) => handleBlur({ lastName: e.target.value })}
          onChange={(e) =>
            setAdditionalInfo({ ...additionalInfo, lastName: e.target.value })
          }
        />
        <p>{error.lastName && error.lastName}</p>
      </div>
      <span>Address</span>
      <div className={`${CLASS}__content__body__form__item`}>
        <input
          placeholder="Street Name"
          maxLength="100"
          value={additionalInfo.street}
          onBlur={(e) => handleBlur({ street: e.target.value })}
          onChange={(e) =>
            setAdditionalInfo({ ...additionalInfo, street: e.target.value })
          }
        />
        <p>{error.street && error.street}</p>
      </div>
      <div className={`${CLASS}__content__body__form__item`}>
        <input
          type="number"
          placeholder="Your House Number"
          value={additionalInfo.houseNum}
          onBlur={(e) => handleBlur({ houseNum: e.target.value })}
          onChange={(e) =>
            setAdditionalInfo({ ...additionalInfo, houseNum: e.target.value })
          }
        />
        <p>{error.houseNum && error.houseNum}</p>
      </div>
      <div className={`${CLASS}__content__body__form__item`}>
        <input
          type="number"
          placeholder="Zip code"
          value={additionalInfo.zip}
          onBlur={(e) => handleBlur({ zip: e.target.value })}
          onChange={(e) =>
            setAdditionalInfo({ ...additionalInfo, zip: e.target.value })
          }
        />
        <p>{error.zip && error.zip}</p>
      </div>

      <span>Country</span>
      <div className={`${CLASS}__content__body__form__item`}>
        <select
          onChange={(e) =>
            setAdditionalInfo({ ...additionalInfo, county: e.target.value })
          }
        >
          <option value="Germany">Germany</option>
          <option value="Austria">Austria</option>
          <option value="Switzerland">Switzerland</option>
        </select>
        <p>{error.country && error.country}</p>
      </div>
    </>
  );
  return (
    <div className={CLASS}>
      <h3>Account Settings</h3>
      <div className={`${CLASS}__content`}>
        <div className={`${CLASS}__content__tabs`}>
          <div
            onClick={() => setActiveTab("mainInfo")}
            className={`${CLASS}__content__tabs__item${
              activeTab === "mainInfo" ? "--active" : ""
            }`}
          >
            Main Information
          </div>
          <div
            onClick={() => setActiveTab("AdditionalInfo")}
            className={`${CLASS}__content__tabs__item${
              activeTab === "AdditionalInfo" ? "--active" : ""
            }`}
          >
            Additional Information
          </div>
        </div>
        <div className={`${CLASS}__content__body`}>
          <div className={`${CLASS}__content__body__headline`}>
            <p>
              {activeTab === "mainInfo"
                ? "Change your E-mail address"
                : "Change your account informations"}
            </p>
          </div>
          <div className={`${CLASS}__content__body__form`}>
            <form onSubmit={(e) => handleMainInfoSubmit(e, dataToSubmit)}>
              {activeTab === "mainInfo" ? mainInfoContent : additionalInfoContent}

              <div className={`${CLASS}__content__body__form__buttons`}>
                <button onClick={() => reset()} type="button">
                  Cancel
                </button>
                <button type="submit">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
const mapDispatchToProps = (dispatch) => ({
  updateUserInfo: (data) => dispatch(updateUserInfo(data)),
});
export default connect(null, mapDispatchToProps)(UserInputTabs);
