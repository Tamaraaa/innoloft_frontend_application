import React from "react";

import Sidebar from "./Sidebar/Sidebar";
import UserInputTabs from "./UserInputTabs/UserInputTabs";

import "./User.scss";

const User = ({ showSidebar ,setShowSidebar}) => {
  const CLASS = "User";
  return (
    <div className={CLASS}>
      <Sidebar  setShowSidebar={setShowSidebar} showSidebar={showSidebar} />
      <UserInputTabs />
    </div>
  );
};
export default User;
