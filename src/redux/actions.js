import * as TYPES from "./action-types";
import axios from "axios";

// --- UPDATE MAIN INFO ----
const updateUserInfoStart = () => {
  return {
    type: TYPES.UPDATE_USER_INFO_START,
  };
};

const updateUserInfoError = (error) => {
  return {
    type: TYPES.UPDATE_USER_INFO_ERROR,
    payload: error,
  };
};

const updateUserInfoEnd = () => {
  return {
    type: TYPES.UPDATE_USER_INFO_END,
  };
};
export const updateUserInfo = (data) => {
  return (dispatch) => {
    dispatch(updateUserInfoStart());
    axios
      .post("updateUserInfo", { data })
      .then((responese) => {
        dispatch(updateUserInfoEnd());
      })
      .catch((error) => {
        dispatch(updateUserInfoError(error.message));
      });
  };
};
