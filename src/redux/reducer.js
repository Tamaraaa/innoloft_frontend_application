import * as TYPES from "./action-types";

const INITIAL_STATE = {
  loading: false,
  error: null,
  success: false,
};
export const rootReducer = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case TYPES.UPDATE_USER_INFO_START:
      return { ...state, loading: true,error:null,success:false };
    case TYPES.UPDATE_USER_INFO_END:
      return { ...state, loading: false,success:true };
    case TYPES.UPDATE_USER_INFO_ERROR:
      return { ...state, loading: false, error: payload };

    default:
      return state;
  }
};
