import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

import { rootReducer } from "./reducer";

const enhancers = [];
let composeEnhancers = compose;
if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === "function") {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
}
const middlewares = [thunk];
const { logger } = require("redux-logger");

middlewares.push(logger);

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(...middlewares), ...enhancers)
);
export { store };
