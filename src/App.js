import React, { useState } from "react";
import { connect } from 'react-redux';

import Header from "./components/Header/Header";
import User from "./components/User/User";
import Notification from './components/Notification/Notification'
import "./App.scss";

const App = ({error}) => {
  const [showSidebar, setShowSidebar] = useState(false);

  return (
    <div className="App">
      <Header setShowSidebar={setShowSidebar} showSidebar={showSidebar} />
      <div className="Dashboard">
       {error &&  <Notification message={error}/>}
        <User setShowSidebar={setShowSidebar} showSidebar={showSidebar} />
      </div>
    </div>
  );
};
const mapStateToProps = state => ({
  error: state.error,
});
export default connect(mapStateToProps,null)(App);
